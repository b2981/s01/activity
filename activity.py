number = [1, 2, 3, 4, 5]

meals = ['spam', 'spam', "SPAM!"]

print(f'list of numbers: {number}')
print(f'list of meals: {meals}')

messages = ["Hi!"]

messages = messages * 4

print(f'Result of repeat operator: {messages}')

print(f"List literals and operations using 'number' list")

print(f"Length of the List: {len(number)}")

print(f'The last item was removed from list: {number[len(number)-1]}')

del number[len(number)-1]

number.reverse()
print(f'The list in reversed order: {number}')

print("List literals and operations using 'meals' list")
print(f'Using indexing operation: {meals[2]}')
print(f'Using negative indexing operation: {meals[-2]}')

meals[-2] = "eggs";
print(f'Modified list: {meals}')
meals.append("bacon")
print(f'Added a new item in the list: {meals}')
meals.sort()
print(f'Modified list after sort method: {meals}')

print(f"Created recipe dictionary:")
recipe = {
	"eggs" : 3
}

print(f' recipe = {recipe}')

recipe["spam"] = 2
recipe["ham"] = 1
recipe["brunch"] = "bacon"

print(f'Modified recipe dictionary :\n {recipe} ')

recipe["ham"] = ["grill", "bake", "fry"]

print(f" Updated the 'ham' key value  : \n{recipe}")

del recipe["eggs"]
print(f"Modified recipe after deleting 'eggs' key  : \n{recipe}")

bob =  {
		'name': {'first': 'Bob', 'last': 'Smith'},
		'age': 42,
		'job': ['software', 'writing'],
        'pay': (40000, 50000)
        }

print(f"Given 'bob' dictionary: \n {bob}")


print(f"Accessing the value of 'name' key: {bob['name']}")

print(f"Accessing the value of 'last' key: {bob['name']['last']}")

print(f"Accessing the second value of pay key: {bob['pay'][1]}")

numeric = ('twelve', 3.0, [11, 22, 33])

print(f'Given "numeric" tuple : \n {numeric}')
print(f'Accessing the tuple item : {numeric[1]}')
print(f'Accessing the tuple item : {numeric[2][1]}')
